const question = document.getElementById('question');
const answerButtons = document.getElementById('answer-buttons');
const startButton = document.getElementById('start-btn');
const skipButton = document.getElementById('skip-btn');
const score = document.getElementById('score');
const questionContainerElement = document.getElementById('container-questions');
const totalScore = document.getElementById('total-prize');
const currentScore = document.getElementById('current');


let questionCounter = 0;
let availableQuestions= [];
let availableOptions= [];
let currentQuestion;
let currentNum;
let totalNum;

startButton.addEventListener('click', startGame);


function startGame() {
  currentNum =100
  totalNum = 0
  totalScore.innerHTML = 'Total prize:' +totalNum
  currentScore.innerHTML = 'Prize on current round:' + currentNum
   skipButton.classList.remove('hide')
   score.classList.remove('hide')
   questionContainerElement.classList.remove('hide')
   answerButtons.classList.remove('hide')
   skipButton.addEventListener('click',() => { 
    skipButton.classList.add('hide')
    getNewQuestion()
   })

   getNewQuestion()
}

function setAvailableQuestion(){
    const totalQuestion = window.questions.length

    for(let i =0; i<totalQuestion; i++){
        availableQuestions.push(window.questions[i])
    }
}

function getNewQuestion(){
const questionIndex = availableQuestions[Math.floor(Math.random()*availableQuestions.length)]
currentQuestion = questionIndex

const index1 = availableQuestions.indexOf(questionIndex)

availableQuestions.splice(index1,1) 

const optionLen = currentQuestion.content.length
for(let i = 0;i<optionLen;i++){
    availableOptions.push(i)
}

answerButtons.innerHTML = ''

for(let i=0;i<optionLen;i++){
    const optonIndex = availableOptions[Math.floor(Math.random()*availableOptions.length)]
    const index2 = availableOptions.indexOf(optonIndex)
    availableOptions.splice(index2,1)
    const option = document.createElement('div');
    option.innerHTML = currentQuestion.content[optonIndex]
    option.id = optonIndex,
    option.className = 'btn',
    answerButtons.appendChild(option),
    option.setAttribute('onclick', 'getResult(this)');
}
questionCounter++
}

function getResult(element) { 
   const id = parseInt(element.id)
   const optionLen = answerButtons.children.length
   for(let i=0; i<optionLen; i++){
       if(parseInt(answerButtons.children[i].id)===currentQuestion.correct){
           answerButtons.children[i].classList.add('correct')
       } else {
         answerButtons.children[i].classList.add('wrong')
       }
   }
   if(id===currentQuestion.correct){
       totalNum +=currentNum
       currentNum *= 2
       totalScore.innerHTML = 'Total prize:' +totalNum
       currentScore.innerHTML = 'Prize on current round:' + currentNum
      if(totalNum >1000000) { 
         question.innerText = 'Congratulations! You won 1 000 000.'
        score.classList.add('hide')
        skipButton.classList.add('hide')
        startButton.classList.remove('hide')
        answerButtons.classList.add('hide')
      } else{
        setTimeout(getNewQuestion, 1000)
      }
   }else{
     setTimeout(endGame, 1000)
   }
}

function endGame(){
  question.innerText = 'Game over. Your prize is: ' + totalNum
  score.classList.add('hide')
  skipButton.classList.add('hide')
  answerButtons.classList.add('hide')
}

window.onload = function(){
    setAvailableQuestion()
    getNewQuestion()
}
