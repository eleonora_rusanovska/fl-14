/*завдання 1*/
function isEquals(arg1, arg2) {
  switch (true) {
    case arg1===arg2: return true;
    case arg1!==arg2: return false;
  }
}

/*завдання 2*/
function numberToString(number) { 
    if(typeof number === 'number') {
        return String(number);
    }
}

/*завдання 3*/
const storeNames = (...str) => {
    let arbitaryStrings = [];
    for (let item of str) {
      arbitaryStrings.push(item);
    }
  return arbitaryStrings;
}

/*завдання 4*/
function getDivision (num1, num2) {
  if(num1>num2) {
      return num1/num2;
  } else {
    return num2/num1;
  }
}

/*завдання 5*/
function negativeCount(arr) {
  let negativeNum = 0;
  arr.forEach(function(x) {
    if (x < 0){
      negativeNum++;
  }
  });
  return console.log(negativeNum);
}


/*завдання 6*/
function letterCount(str1, str2) {
  let counter = 0;
  for(let i=0; i<str1.length; i++) {
    if(str1.charAt(i)===str2){
      counter+=1;
    }
  }
  return counter;
}

/*завдання 7*/
function countPoints(points) {
  let counter = 0;
  points.forEach(p => {
          let splited = p.split(':')
          let x = parseInt(splited[0])
          let y = parseInt(splited[1])
          if(x > y){
            counter+=3;
          } else if(x<y){
            counter+=0;
          } else {
            counter+=1;
          }
      })
  console.log(counter)
  } 