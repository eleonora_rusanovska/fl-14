/*Завдання 1 
convert('1', 2, 3, '4') // [1, '2', '3', 4]*/
const convert = (...list) => {
    const convertedArr = [];

    for (let item of list) {
        if (typeof item === 'number') {
            convertedArr.push(String(item));
        } else {
            convertedArr.push(Number(item));
        }
    }

    return convertedArr;
};
console.log(convert('1', 2, 3, '4'));


/*Завдання 2 
executeforEach([1,2,3], function(el) {console.log(el * 2)}) // 2 4 6 */
function executeforEach(arr, func){
	for(let value of arr){
		func(value);	
	}
}


/*Завдання 3 
mapArray([2, '5', 8], function(el) {return el + 3}) // returns [5, 8, 11]*/
function mapArray (arr, func) {
    let elements = [];
    for(let value of arr) {
        elements.push(func(+value));
     }
    return elements;
}


/*Завдання 4 
filterArray([2, 5, 8], function(el) { return el % 2 === 0 }) 
// returns [2, 8]
*/
function filterArray (arr, func) {
    let param = [];
    for(let value of arr) {
        if(func(value)) {
             param.push(value) 
            }
    }
    return param;
}


/*Завдання 5 
getValuePosition([2, 5, 8], 8)  // returns 3 
getValuePosition([12, 4, 6], 1)  // returns false
*/
function getValuePosition(arr, value){
    for(let i = 0;i<arr.length;i++){
      if(arr[i]===value){
        i++
        return i;
        }
      }return false;
    }


/*Завдання 6 
Write a function that reverses the string value passed into it*/

function flipOver(str) {
    let string = "";
    for (let i = str.length - 1; i >= 0; i--) {
        string += str[i];
    }
    return string;
}
flipOver('hey world!');


/*Завдання 7 
Write a function which creates an array from the given range of numbers*/

function makeListFromRange(arr) {
    let numbers = [];
    for(let i = arr[0]; i <= arr[1]; i++) {
        numbers.push(i);
    }
    for (let i = arr[0]; i>= arr[1]; i--) {
      numbers.push(i);
    }
  return numbers;
}


/*Завдання 8 
console.log(getArrayOfKeys(fruits, ‘name’); 
// returns [‘apple’, ‘pineapple’]
*/
const fruits = [
    { name: 'apple', weight: 0.5 },
    { name: 'pineapple', weight: 2 }
    ];  
function getArrayOfKeys(arr, keys){
    const newArr=[];
    executeforEach(arr, function(item){
        newArr.push(item[keys])
    })
    return newArr;
  }

console.log(getArrayOfKeys(fruits, 'name'));

/*Завдання 9 */

const basket = [
    { name: 'Bread', weight: 0.3 },
    { name: 'Coca-Cola', weight: 0.5 },
    { name: 'Watermelon', weight: 8 }
    ];
function getTotalWeight(arr){
    let tWeight=0;
    executeforEach(arr, function(item){
        for(let prop in item){
            if(!isNaN(item[prop])){
                tWeight +=item[prop]
            }
        }
    })
    return tWeight;
}
console.log(getTotalWeight(basket));


/*Завдання 10 */
//function getPastDay
const date = new Date(2020, 0, 2);

function getPastDay(date, daysN) {

let daysSec = 86400000;
let newDate = new Date(date - daysN*daysSec);
let months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    
return console.log(months[newDate.getMonth()]+' '+newDate.getDate()+' '+newDate.getFullYear()+' ');
}
getPastDay(date, 1); // 1, (1 Jan 2020)
getPastDay(date, 2); // 31, (31 Dec 2019)
getPastDay(date, 365); // 2, (2 Jan 2019)


/*Завдання 11 
Write a function that formats a date in such format "YYYY/MM/DD HH:mm".*/


function addZeroBefore(n) {
  return (n < 10 ? '0' : '') + n;
}

function formatDate(dateIn) {

let date = dateIn;
if (dateIn === undefined) {
date = new Date()
let output = date.getFullYear()+'/'+addZeroBefore(date.getMonth() + 1)+'/'+
 addZeroBefore(date.getDate())+' '+addZeroBefore(date.getHours())+':'+addZeroBefore(date.getMinutes());
return console.log(output);
} else {
let date = new Date(dateIn);
let output = date.getFullYear()+'/'+addZeroBefore(date.getMonth() + 1)+'/'+
 addZeroBefore(date.getDate())+' '+addZeroBefore(date.getHours())+':'+addZeroBefore(date.getMinutes())
return console.log(output);
}

}

formatDate(new Date('6/15/2019 09:15:00'));
formatDate(new Date())