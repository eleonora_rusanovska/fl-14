//task1 
const birthday22 = new Date(2000, 9, 22);
const birthday23 = new Date(2000, 9, 29);
const dateNow = new Date();

function getAge(date) {
let a = dateNow.getFullYear();
let b = date.getFullYear();
let c = a - b;

if (dateNow.getMonth() < date.getMonth()) {
console.log(c)
} else if (dateNow.getMonth() === date.getMonth() && dateNow.getDate() <= date.getDate()) {
console.log(c)
} else {
console.log(c - 1);
}
}
//getAge(birthday22);

//task2
function getWeekDay (date) {
let days =['Saturday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
let d = new Date(date);
let today = days[d.getDay()];
console.log(today);
}

//getWeekDay(new Date(2020, 9, 22)); 
//getWeekDay(Date.now());

//task3
function getProgrammersDay(year) {
const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
if (year === 1918) {
return console.log(`26.09.1918`); 
} else if (year > 1918) {
if(year % 4 === 0 && year % 100 !== 0 || year % 400 === 0) {
let d = new Date(year, 12, 9);
let dayName = days[d.getDay()];
return console.log(`12 Sep, ${year} (${dayName})`);
}else{
let d = new Date(year, 13, 9-2);
let dayName = days[d.getDay()];
return console.log(`13 Sep, ${year} (${dayName})`);
}
}
}
 //getProgrammersDay(2020); // "12 Sep, 2020 (Saturday)"
 //getProgrammersDay(2019); // "13 Sep, 2019 (Friday)"

//task4
function howFarIs(str) {
let days =['Saturday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
let d = new Date();
let today = days[d.getDay()];

let reg = str.match(/^(?:[a-zA-Z])+ {1,}$/i);
let replace= str.replace(reg);
let specifiedWeekday =replace.charAt(0).toUpperCase()+replace.slice(1);

if (today === specifiedWeekday) {
console.log(`Hey, today is ${ specifiedWeekday } =)`);
} else {
console.log(`It's day(s) left till ${ specifiedWeekday }`)
}
}
//howFarIs('wednesday');

//task5
function isValidIdentifier(identifier) { 
if(identifier.match(/^[^\0-9](?:[a-zA-Z0-9/_/$/.]){1,}$/) ) { 
return console.log('true')
} else { 
return console.log('false')
}
}

//isValidIdentifier('myVar_1'); // true
//isValidIdentifier('1_myVar'); // false

//task6
const testStr = 'My name is John Smith. I am 27.';

function upperCase(str) {
return str.toUpperCase(); 
}

function capitalize(str) {
let firstL = /(^|\s)[a-z]/g;
return console.log(str.replace(firstL, upperCase));
}
//capitalize(testStr); 

//task7
function isValidAudioFile(audiofile) { 
if(audiofile.match(/^(?:[a-zA-Z0-9.])+(?:alac|mp3|flac|acc){1,}$/) ) { 
return console.log('true')
} else { 
return console.log('false')
}
}
//isValidAudioFile('file.mp4'); // false
//isValidAudioFile('my_file.mp3'); // false
//isValidAudioFile('file.mp3'); // true

//task8
const testString = 'color: #3f3; background-color: #AA00ef; and: #abcd';
function getHexadecimalColors(x) {
let reg = x.match(/#([a-f0-9]{3}){1,2}\b/gi);
if (reg !== null) {
return console.log(reg);
} else {
console.log([]);
}
}
//getHexadecimalColors(testString);
//getHexadecimalColors('red and #0000');

//task9
function isValidPassword(inputtxt) { 
if(inputtxt.match(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}$/)) { 
return console.log('true')
} else { 
return console.log('false')
}
}
//isValidPassword('agent007'); // false (no uppercase letter)
//isValidPassword('Agent007'); // true

//Task10
function addThousandsSeparators(x) {
return console.log(x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ','));
}

//addThousandsSeparators('1234567890'); // "1,234,567,890"
//addThousandsSeparators(1234567890); // "1,234,567,890"