function getAmount() {
    
        let batteriesAmount = parseFloat(prompt('Please enter amount of batteries on a factory:'));
        let defect = parseFloat(prompt('Please enter percentage of defective batteries:'));
        let defectBat = parseFloat(batteriesAmount * defect / 100).toFixed(2);
        if (isNaN(batteriesAmount) || isNaN(defect)) {
            alert("Invalid input data");
        } else if (defect < 0 || batteriesAmount < 0 || defect > 100) {
            alert("Invalid input data ");
        } else {
            alert("Amount of defective batteries: " + defectBat +
            "\nAmount of working batteries: " + (batteriesAmount - defectBat).toFixed(2));
        }
    
 }

    getAmount()