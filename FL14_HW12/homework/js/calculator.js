while (true) {
    let request = prompt('Please, enter the expression');
    if (request == null) break;
  
    try {
      let result = eval(request);
      if (isNaN(result)) {
        prompt( "Error, please try again:" );
      }
  
      break;
    } catch (e) {
      prompt( "Error: " + e.message + ", please try again:" );
    }
  }
  
  alert(result);
