function visitLink(path) {
  let count = localStorage.getItem(path);
  if (count ===null) {
  count = 0;
  localStorage.setItem(path, count);
  count = parseInt(count);
  } else {
  count +=1;
  localStorage.setItem(path, count);
  count = parseInt(count);
  }
  }
  
  function viewResults() {
  let wrapper = document.createElement("UL"); 
  wrapper.id = 'ul';                   
   result1 = document.createElement("LI"), 
   result2 = document.createElement("LI"),
   result3 = document.createElement("LI"), 
   text1 = document.createTextNode('You visited Page1', ${localStorage.Page1}, 'time(s)'), 
   text2 = document.createTextNode('You visited Page2', ${localStorage.Page2}, 'time(s)'), 
   text3 = document.createTextNode('You visited Page3', ${localStorage.Page3}, 'time(s)'),  
  result1.appendChild(text1),   
  result2.appendChild(text2),
  result3.appendChild(text3),                                     
  document.getElementById('content').appendChild(wrapper),
  document.getElementById('ul').appendChild(result1),
  document.getElementById('ul').appendChild(result2),
  document.getElementById('ul').appendChild(result3),
  localStorage.clear();
  }

